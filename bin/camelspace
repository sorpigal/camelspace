#!/usr/bin/perl
use strict;
use warnings;

use v5.12;

my $app = 'camelspace';

sub usage () {
	<<HELP
Usage: $app [-ult] [-c FORMAT] WORD [WORD...]
Convert each WORD into kebab case.

  -c, --case FORMAT    Instead of kebab case convert to FORMAT, which must be
                       one of kebab, snake, camel, pascal, or space
  -u, --upper          Converted words will be in upper case
  -l, --lower          Converted words will be in lower case (default)
  -t, --title          Converted words will be in title case
      --preserve-case  Do not convert input words into lower case
  -h, --help           display this help and exit
      --version        output version information and exit

Use of --upper, --lower, --title, and --preserve-case are mutually exclusive
with each other.

The default output format is lower kebab case.
HELP
}

sub version () {
	say '0.0.3';
}

sub try_help () {
	"Try '$app --help' for more information."
}

# turn aCamelCaseString (or aCamelCASEString) into
# ['a', 'camel' 'CASE' 'String']; also works if the first word begins with
# an upper case character (pascal case)
sub camelcase_to_words {
	grep { defined } shift =~ /([^[:upper:]]+(?![^[:upper:]]))|([[:upper:]]+(?![^[:upper:]]))|([[:upper:]]?[^[:upper:]]+)/g;
}

# turn a-kebab-case-string into ['a', 'kebab', 'case', 'string']
sub kebabcase_to_words {
	split /-/, shift;
}

# turn a_snake_case_string into ['a', 'snake', 'case', 'string']
sub snakecase_to_words {
	split /_/, shift;
}

# turn a space-delimited string into a list of words
sub spacecase_to_words {
	split /\s+/, shift;
}

# trim each word in the argument list
sub trim_words {
	map { s/^\s+//; s/\s+$//; $_; } @_;
}

# turn ['list', 'Of', 'WORDS'] into ['list', 'of', 'words']
sub words_to_lowercase {
	map { lc } @_;
}

# turn ['list', 'Of', 'WORDS'] into ['LIST', 'OF', 'WORDS']
sub words_to_uppercase {
	map { uc } @_;
}

# turn ['list', 'of', 'words'] into ['List', 'Of', 'Words']
sub words_to_titlecase {
	map { ucfirst(lc) } @_;
}

# allow acronyms in camel/pascal words when unambiguous
sub camel_words_to_titlecase {
	my $uh_oh = shift;
	my $uc;
	map {
		$_ = ucfirst(lc) if $uh_oh ne 'chaos' && $uc;
		$uc = $_ =~ /[[:upper:]]$/;
		ucfirst($_);
	} @_;
}

# turn ['list', 'of', 'words'] into ['list', 'Of', 'Words']
sub words_to_camelcase {
	my @words = camel_words_to_titlecase(@_);
	$words[0] = lcfirst($words[0]);
	return @words;
}

# turn ['list', 'of', 'words'] into ['List', 'Of', 'Words']
sub words_to_pascalcase {
	camel_words_to_titlecase(@_);
}

# report format the string appears to be in so we can select the right parser
sub detect_case {
	local $_ = shift;
	if (/-/) {
		'kebab';
	} elsif (/_/) {
		'snake';
	} elsif (/ /) {
		'space';
	} else {
		# either camel case or pascal case; parsed the same way
		'camel';
	}
}

sub anycase_to_words {
	my $term = shift;
	my $case = detect_case($term);

	if ($case eq 'kebab') {
		kebabcase_to_words($term);
	} elsif ($case eq 'snake') {
		snakecase_to_words($term);
	} elsif ($case eq 'space') {
		spacecase_to_words($term);
	} elsif ($case eq 'camel') {
		camelcase_to_words($term);
	} else {
		die "unhandled word case: $case\n"
	}
}


my $cow;

sub emit {
	my $output = shift;
	say $output unless $cow;
	return unless $cow;
	my $l = length($output) + 2;
	printf(" %s\n< %s >\n %s\n", '_' x $l, $output, '-' x $l);
say <<'MOO'
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||
MOO
}

my $moo = 0;

sub moo {
	return unless $moo;
	my @cow = (
		sub {
			say "$app has super cow powers";
			exit;
		},
		undef,
		sub {
			say "I'm serious, cow powers";
			exit;
		},
		sub {
			say "last warning...";
			exit;
		},
		sub {
			$cow = 1;
		},
	);
	my $code;
	for(my $i=0;$i<$moo;$i++) {
		$code = $cow[$i] if defined $cow[$i];
	}

	$code = sub {
		exit(1);
	} unless defined $code;

	&{$code};
}

sub unbundle {
	map { sprintf('-%s', $_) } split(//, substr(shift, 1));
}

sub bad_option {
	sprintf("unrecognized option '%s'\n", shift);
}

my $help = 0;
my $version = 0;

# one of lc, uc, tc, off
my $case_fold = 'lc';

# one of kebab, snake, camel, pascal, space
my $to = 'kebab';

my $original_arg;
my $unbundled = 0;

use Data::Dumper qw/Dumper/;

my @terms;
while ($#ARGV > -1) {
	my $arg = shift;

	if ($arg eq '-h' || $arg eq '--help') {
		$help = 1;
	} elsif ($arg eq '--version') {
		$version = 1;
	} elsif ($arg eq '-c' || $arg eq '--case') {
		$to = shift;
		unless (
			$to eq 'kebab' || $to eq 'snake' ||
			$to eq 'camel' || $to eq 'pascal' ||
			$to eq 'space'
		) {
			die "$app: $arg: unrecognized case: $to\n"
		}
	} elsif ($arg eq '-l' || $arg eq '--lower') {
		$case_fold = 'lc';
	} elsif ($arg eq '-u' || $arg eq '--upper') {
		$case_fold = 'uc';
	} elsif ($arg eq '-t' || $arg eq '--title') {
		$case_fold = 'tc';
	} elsif ($arg eq '--preserve-case') {
		$case_fold = 'off';
	} elsif ($arg eq '--chaos-ensues') {
		# stricter ---preserve-case that can produce a string that can't be parsed back in to the same words
		$case_fold = 'chaos';
	} elsif ($arg eq '--moo') {
		$moo++;
	} elsif ($arg eq '--') {
		@terms = (@terms, @ARGV);
		break;
	} elsif ($arg =~ /^--.*/) {
		die bad_option $arg;
	} elsif ($arg =~ /^-.*/) {
		if (!$unbundled && length($arg) > 2) {
			$original_arg = $arg;
			@ARGV  = (unbundle($arg), @ARGV);
			$unbundled = length($arg);
		} else {
			die bad_option $original_arg // $arg;
		}
	} else {
		push(@terms, $arg);
	}
	$unbundled--;
	if ($unbundled < 0) {
		$unbundled = 0;
		undef $original_arg;
	}
}

if ($help) {
	say usage;
	exit;
} elsif ($version) {
	version;
	exit;
}

moo;

sub convert_term {
	my $term = shift;

	my @words = anycase_to_words($term);

	if ($case_fold eq 'lc') {
		@words = words_to_lowercase(@words);
	} elsif ($case_fold eq 'uc') {
		@words = words_to_uppercase(@words);
	} elsif ($case_fold eq 'tc') {
		@words = words_to_titlecase(@words);
	} else {
		# preserve original case (unless $to is camel/pascal)
	}

	@words = trim_words(@words);

	if ($to eq 'kebab') {
		emit join '-', @words;
	} elsif ($to eq 'snake') {
		emit join '_', @words;
	} elsif ($to eq 'camel') {
		emit join '', words_to_camelcase($case_fold, @words);
	} elsif ($to eq 'pascal') {
		emit join '', words_to_pascalcase($case_fold, @words);
	} elsif ($to eq 'space') {
		emit join ' ', @words;
	} else {
		die "bad to case $to";
	}
}

my $any = 0;

foreach my $term (@terms) {
	convert_term($term);
	$any++;
}

unless ( -t STDIN) {
	while (<>) {
		chomp;
		convert_term($_);
	}
	$any++;
}

unless ($any) {
	say shift @{[ split /\n/, usage ]};
	say try_help;
	exit 1;
}
