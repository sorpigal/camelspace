## camelspace

On the command line convert from strings of words delimited with spaces, underscores, hyphens, or via capitalization as with camel or pascal case to strings delimited with any of those things.

### Examples

By default the output  is kebab case, the best case:

```
$ camelspace toLowerCase 'hello world' border-right-color
to-lower-case
hello-world
border-right-color
```

But you can request another format:

```
$ camelspace -c camel toLowerCase 'hello world' border-right-color
toLowerCase
helloWorld
borderRightColor
```

It's possible for `camelspace` to consume its own output as input:

```
$ camelspace hail-eris -c pascal | camelspace -c space -t \
  | camelspace -c snake -u | camelspace
hail-eris
```
